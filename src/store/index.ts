import { Commit } from 'vuex'

export const state = () => ({
  sideBarOpen: true,
})
export type RootState = ReturnType<typeof state>
export const getters = {
  sideBarOpen: (state: RootState) => {
    return state.sideBarOpen
  },
}
export const mutations = {
  TOGGLE_SIDEBAR(state: RootState) {
    state.sideBarOpen = !state.sideBarOpen
  },
}
export const actions = {
  toggleSidebar({ commit }: { commit: Commit }) {
    commit('TOGGLE_SIDEBAR')
  },
}
