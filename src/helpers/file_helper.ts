export function base64(file: any, callback: any) {
  const reader = new FileReader()

  reader.addEventListener('load', () => callback(reader.result))

  reader.readAsDataURL(file)
}
export const getBase64 = (file: any) =>
  new Promise(function (resolve, reject) {
    const reader = new FileReader()
    reader.readAsDataURL(file)
    reader.onload = () => resolve(reader.result)
    reader.onerror = function (error) {
      reject(error)
    }
  })
