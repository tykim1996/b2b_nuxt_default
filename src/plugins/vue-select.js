import Vue from 'vue'
import 'vue-select/dist/vue-select.css'
import { VueSelect } from 'vue-select'

Vue.component('VueSelect', VueSelect)
