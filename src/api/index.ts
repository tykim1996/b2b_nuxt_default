import { NuxtAxiosInstance } from '@nuxtjs/axios'
import posts from './posts'
import units from '~/api/units'

export default ($axios: NuxtAxiosInstance) => ({
  posts: posts($axios),
  units: units($axios),
})
