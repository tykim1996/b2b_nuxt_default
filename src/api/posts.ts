import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { IShowParams } from '~/types'

export default ($axios: NuxtAxiosInstance) => ({
  index() {
    return $axios.$get(`/api/v1/posts`)
  },
  show({ id }: IShowParams) {
    return $axios.$get(`/api/v1/posts/${id}`)
  },
})
