import { NuxtAxiosInstance } from '@nuxtjs/axios'
import { ISearchParams, IShowParams, IUnitNew } from '~/types'

const makeUnitToJson = (unit: IUnitNew) => {
  return {
    unit_name: unit.unitName,
    unit_code: unit.unitCode,
    unit_description: unit.unitDescription,
    sub_unit: unit.subUnit,
    edited_by: unit.editedBy,
    is_active: unit.isActive,
  }
}

export default ($axios: NuxtAxiosInstance) => ({
  index({ page = 0, keyword }: ISearchParams) {
    let url = `/v1/units?page=${page}`
    if (keyword) {
      url += `&keyword=${keyword}`
    }
    return $axios.$get(url)
  },
  show({ id }: IShowParams) {
    return $axios.$get(`/v1/unit/${id}`)
  },
  add(unit: IUnitNew) {
    console.log(unit)
    return $axios.$post(`/v1/unit`, makeUnitToJson(unit))
  },
  update(unit: IUnitNew) {
    return $axios.$put(
      `/v1/unit`,
      Object.assign({ id: unit.id }, makeUnitToJson(unit))
    )
  },
})
