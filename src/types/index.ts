export * from './api'

export interface IBase {
  id: number
  name: string
}

export interface IPost {
  body: string
  id: number
  title: string
  userId: number
}

export interface IGroup extends IBase {
  code: string
  status: string
  image: string
}

export interface IAddAction {
  name: string
  path: string
}

export interface IVariant {
  variant: IBase
  unit?: IBase
}

export interface ICategory extends IBase {
  slug: string
}

export interface IBrand extends IBase {
  description: string
}

export interface IKind extends IBase {
  description: string
  image: string
}

export interface IUnit extends IBase {
  group: IBase
}

export interface IUnitNew {
  id?: number
  unitCode: string
  unitName: string
  unitDescription: string
  subUnit: string
  editedBy: string
  isActive: boolean | true
}

export interface IAttribute extends IBase {
  description: string
  attributeType: string
  unit: IUnit
}

export interface IProduct {
  id: string
  name: string
  brand: string
  group: string
  xuatxu: string
  baohanh: string
  donvi: string
  sl1: string
  sl2: string
  gia1: string
  gia2: string
}

export interface IMetadata {
  total: number
  limit: number
  offset: number
  page: number
  pages: number
}
