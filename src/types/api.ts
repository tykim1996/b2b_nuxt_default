export interface IShowParams {
  id: number
}

export interface ISearchParams {
  keyword: string | null
  page: number | 0
}
