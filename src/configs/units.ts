import { IUnit } from '~/types'

const Units = [
  { id: 1, name: 'Cm', group: { id: 1, name: 'Đơn chị chiều dài' } },
  { id: 2, name: 'Cuộn', group: { id: 2, name: 'Đơn chị khác' } },
  { id: 3, name: 'Chiếc', group: { id: 2, name: 'Đơn chị khác' } },
  { id: 4, name: 'Vòng', group: { id: 2, name: 'Đơn chị khác' } },
] as IUnit[]

export default Units
