import { IAttribute } from '~/types'

const Properties = [
  {
    id: 1,
    name: 'Đầu test',
    attributeType: 'Nhập số',
    unit: { id: 1, name: 'Cm', group: { id: 1, name: 'Đơn chị chiều dài' } },
    description: '',
  },
  {
    id: 2,
    name: 'Size đồng hồ',
    attributeType: 'Nhập text',
    description: '',
  },
  {
    id: 3,
    name: 'Dải áp ( Bar)',
    attributeType: 'Chọn trong danh sách',
    description: '',
  },
  {
    id: 4,
    name: 'Vật liêu',
    attributeType: 'Nhập text ',
    description: '',
  },
  {
    id: 5,
    name: 'Phụ kiện',
    attributeType: 'Nhập số',
    description: '',
  },
] as IAttribute[]

export default Properties
