import { IKind } from '~/types'

const Kinds = [
  {
    id: 1,
    name: 'Đầu nối hệ DIN',
    description: 'Đầu nối hệ DIN',
    image: '/img/photo1.png',
  },
  {
    id: 2,
    name: 'Đầu nối hệ thẳng',
    description: 'Thương hiệu hàng đầu về thủy lực ở Việt Nam',
  },
  {
    id: 3,
    name: 'Máy bơm chìm',
    description: 'Chuyên gia công cổ phốt, xy lanh, phụ kiện xy lanh',
  },
  { id: 5, name: 'Noga', image: '/img/photo3.jpg' },
  {
    id: 6,
    name: 'PETROLIMEX',
    description: 'Some description',
    image: '/img/photo2.png',
  },
] as IKind[]
export default Kinds
