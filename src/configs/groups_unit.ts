import { IBase } from '~/types'

const UnitGroups = [
  { id: 1, name: 'Đơn chị chiều dài' },
  { id: 2, name: 'Đơn chị khác' },
] as IBase[]

export default UnitGroups
