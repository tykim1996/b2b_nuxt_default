const Groups = [
  {
    id: 1,
    code: 'CAT-001',
    name: 'Sản phẩm nhóm 1',
    status: 'warning',
    image: '/img/prod-1.jpg',
  },
  {
    id: 2,
    code: 'CAT-002',
    name: 'Sản phẩm nhóm 2',
    status: 'success',
    image: '/img/prod-2.jpg',
  },
  {
    id: 3,
    code: 'CAT-003',
    name: 'Sản phẩm nhóm 3',
    status: 'draft',
    image: '/img/prod-3.jpg',
  },
  {
    id: 4,
    code: 'CAT-004',
    name: 'Sản phẩm nhóm 4',
    status: 'success',
    image: '/img/prod-4.jpg',
  },
  {
    id: 5,
    code: 'CAT-005',
    name: 'Sản phẩm nhóm 6',
    status: 'delete',
    image: '/img/prod-5.jpg',
  },
]

export default Groups
