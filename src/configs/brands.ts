import { IBrand } from '~/types'

const Brands = [
  {
    id: 1,
    name: 'OEM',
    description: 'OEM là những nhà gia công chưa có thương hiệu',
    image: '/img/photo1.jpg',
  },
  {
    id: 2,
    name: 'Việt Hà',
    description: 'Thương hiệu hàng đầu về thủy lực ở Việt Nam',
  },
  {
    id: 3,
    name: 'MeKong',
    description: 'Chuyên gia công cổ phốt, xy lanh, phụ kiện xy lanh',
  },
  { id: 4, name: 'Loctile' },
  { id: 5, name: 'Noga' },
  {
    id: 6,
    name: 'PETROLIMEX',
    description: 'Some description',
  },
] as IBrand[]
export default Brands
