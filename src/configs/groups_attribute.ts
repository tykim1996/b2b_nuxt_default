import { IBase } from '~/types'

const AttributeGroups = [
  { id: 1, name: 'Nhóm thuộc tính 1' },
  { id: 2, name: 'Nhóm thuộc tính 2' },
] as IBase[]

export default AttributeGroups
