export default [
  { name: 'Danh mục', isHeader: true },
  {
    name: 'Bảng tin',
    link: '/',
    icon: 'fa-home',
  },
  { name: 'Nhà bán', isHeader: true },
  {
    name: 'Sản phẩm nhóm',
    link: '/groups',
    icon: 'fas fa-inbox',
  },
  {
    name: 'Sản phẩm',
    link: '/products',
    icon: 'fas fa-box',
  },
  {
    name: 'Thương hiệu',
    link: '/brands',
    icon: 'fas fa-code-branch',
  },
  {
    name: 'Kiểu sản phẩm',
    link: '/kinds',
    icon: 'fas fa-industry',
  },
  {
    name: 'Chủng loại',
    link: '/types',
    icon: 'fa-plus-square',
  },
  {
    name: 'Thuộc tính',
    link: '/attributes',
    icon: 'fas fa-tasks',
  },
  { name: 'Hệ thống', isHeader: true },
  {
    name: 'Thành viên',
    link: '/users',
    icon: 'fa-users',
  },
  {
    name: 'Danh mục',
    link: '/categories',
    icon: 'fas fa-border-all',
  },
  {
    name: 'Đơn vị',
    link: '/units',
    icon: 'fas fa-suitcase-rolling',
  },
]
