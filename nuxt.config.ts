import { NuxtConfig } from '@nuxt/types'

const config: NuxtConfig = {
  // Global page headers: https://go.nuxtjs.dev/config-head
  ssr: true,
  head: {
    title: 'Nuxt',
    titleTemplate: 'B2b Sale Center | %s',
    htmlAttrs: {
      lang: 'en',
      class: ['antialiased'],
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
  },

  loading: {
    color: '#0ea5e9',
  },
  server: {
    host: '0.0.0.0', // default: localhost
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: ['~assets/scss/style.scss'],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/axios',
    '~/plugins/api',
    { src: '~/plugins/vue-tree-select.js', ssr: false },
    { src: '~/plugins/vue-select.js' },
    { src: '~/plugins/vue-js-modal.js' },
  ],
  srcDir: 'src/',
  // Auto import components: https://go.nuxtjs.dev/config-components
  components: false,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://vite.nuxtjs.org/get-started/usage
    // 'nuxt-vite', // ERROR  'defineComponent' is not exported by .nuxt/composition-api/entrypoint.js, imported by layouts/default.vue?vue&type=script&lang.ts
    // https://composition-api.nuxtjs.org/getting-started/setup
    '@nuxtjs/composition-api/module',
    // https://go.nuxtjs.dev/typescript
    '@nuxt/typescript-build',
    // https://go.nuxtjs.dev/stylelint
    '@nuxtjs/stylelint-module',
    // https://image.nuxtjs.org/setup
    '@nuxt/image',
    // https://google-fonts.nuxtjs.org/setup
    '@nuxtjs/google-fonts',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://auth.nuxtjs.org/guide/setup
    '@nuxtjs/auth-next',
    // https://i18n.nuxtjs.org/setup
    'nuxt-i18n',
    // https://www.npmjs.com/package/@nuxtjs/device
    '@nuxtjs/device',
  ],
  auth: {
    // Options
  },

  serverMiddleware: [
    {
      path: '/api/v1',
      handler: '~/server/rest-api',
    },
  ],

  vite: {
    /* options for vite */
    vue: {
      /* options for vite-plugin-vue2 */
    },
  },

  image: {},

  i18n: {},

  googleFonts: {
    download: true,
    display: 'swap', // 'auto' | 'block' | 'swap' | 'fallback' | 'optional'
    families: {
      Inter: [100, 200, 300, 400, 500, 600, 700, 800, 900],
    },
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'http://45.77.37.85:5000',
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {},
}
export default config
