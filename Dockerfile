FROM node:lts-alpine
# install simple http server for serving static content
RUN yarn --version
RUN yarn global add pm2
# make the 'app' folder the current working directory
WORKDIR /app
# copy both 'package.json' and 'package-lock.json' (if available)
COPY package*.json ./
# install project dependencies
RUN yarn install
# copy project files and folders to the current working directory (i.e. 'app' folder)
COPY . .
# build app for production with minification
RUN yarn build
EXPOSE 3000
CMD [ "yarn", "start"]
