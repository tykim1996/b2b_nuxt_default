module.exports = {
  apps: [
    {
      name: 'b2b_sale_center',
      exec_mode: 'cluster',
      port: 4000,
      // instances: 'max', // Or a number of instances
      script: './node_modules/.bin/nuxt',
      args: 'start',
      instances: 2,
      wait_ready: true,
      listen_timeout: 5000,
    },
  ],
}
